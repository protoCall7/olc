# olc
> An Ohm's Law calculator webapp

OLC is an Ohm's Law calculator webapp implemented using the BCHS stack. Main project goals include security, performance, and RESTful API completeness.

## Installing / Getting Started

OLC is designed to run on the OpenBSD httpd(8) daemon. See the Initial Configuration section for details on httpd(8)/slowcgi(8) configuration. Basic installation on a pre-configured machine consists of building and installing the application, and starting the httpd(8) and slowcgi(8) daemons.
```shell
cd olc
make
doas make install
doas rcctl start httpd
doas rcctl start slowcgi
```

The example above builds OLC from the top level of the source directory, installs it into the webroot, and starts the httpd(8) and slowcgi(8) daemons.

## Initial Configuration

Before starting httpd, a configuration must be in place to direct calls to the application to the slowcgi(8) daemon.
```
server "default" {
	listen on * port 80
	root "/htdocs"
	location "/cgi-bin/olc/*" {
		fastcgi
		root "/"
	}
}
```

The above config snippet is the minimum required configuration required to start the server. The configuration file should be placed at `/etc/httpd.conf` before attempting to start the daemon. See httpd.conf(5) for more information.

If OLC is to run on the server long-term, it is a good idea to "enable" the httpd(8) and slowcgi(8) services, so that they will start automatically at system boot
```shell
doas rcctl enable httpd
doas rcctl enable slowcgi
```

## Contributing
If you'd like to contribute, please fork the repository and use a feature branch. Merge requests are warmly welcome.

## Links

- BCHS: https://learnbchs.org 
- OpenBSD: https://openbsd.org
- httpd: https://bsd.plumbing

## License
OLC is licensed under the BSD 3-Clause license. Please see the LICENSE file for more information.